package com.kkulpan.rsoprojekt.rsoprojekt;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

class RSOApi {

    private static final String URL = "https://komunikaty.tvp.pl/komunikatyxml";

    StringBuilder POST(String voivodeship, String info_kind, String pagination) {
        StringBuilder stringBuilder = new StringBuilder();
        try {
            URL url = new URL(URL + "/" + voivodeship + "/" + info_kind + "/" + pagination + "?_format=json");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setConnectTimeout(5000);
            conn.setReadTimeout(5000);
            if (conn.getErrorStream() != null) {
                System.out.println("Nie udalo sie pobrac danych");
            }

            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(conn.getInputStream()));

            String output;
            while ((output = bufferedReader.readLine()) != null) {
                stringBuilder.append(output);
                stringBuilder.append("\n");
            }
        } catch (IOException e) {
            stringBuilder.append(e.getMessage());
        }
        return stringBuilder;
    }

}


