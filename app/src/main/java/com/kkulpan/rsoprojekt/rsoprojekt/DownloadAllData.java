package com.kkulpan.rsoprojekt.rsoprojekt;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.osmdroid.config.Configuration;

import java.util.Set;

public class DownloadAllData extends AppCompatActivity implements AsyncResponse {

    public static boolean sharedPreferencesHasChanged = false;
    public static String CHANNEL_ID = "rsoProjekt";
    SharedPreferences sharedPreferences;
    Core core;
    Set<String> selected_voivodeships;
    Set<String> selected_info_kinds;
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private JSONArray data = new JSONArray();

    private void createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = "rsoProjekt";
            String description = "Powiadomienia rsoProjekt";
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, name, importance);
            channel.setDescription(description);
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.info_dashboard);

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        createNotificationChannel();
        setTitle("Komunikaty");

        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);

        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        Configuration.getInstance().load(getApplicationContext(), PreferenceManager.getDefaultSharedPreferences(getApplicationContext()));
        Configuration.getInstance().setUserAgentValue(getPackageName());

        selected_voivodeships = sharedPreferences.getStringSet("message_voivodeship_set", null);
        selected_info_kinds = sharedPreferences.getStringSet("message_info_kind_set", null);

        if (selected_voivodeships != null && selected_info_kinds != null) {
            data = new JSONArray();
            for (String voivodeship : selected_voivodeships) {
                for (String info_kind : selected_info_kinds) {

                    core = new Core();
                    core.asyncResponse = this;
                    core.execute(voivodeship, info_kind, "0");
                }
            }
            mRecyclerView.setAdapter(null);
            mLayoutManager = new LinearLayoutManager(this);
            mRecyclerView.setLayoutManager(mLayoutManager);
            mRecyclerView.setDrawingCacheEnabled(true);
            mRecyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
            Toast toast = Toast.makeText(this, "Pobieram informację w tle...", Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (sharedPreferencesHasChanged) {
            selected_voivodeships = sharedPreferences.getStringSet("message_voivodeship_set", null);
            selected_info_kinds = sharedPreferences.getStringSet("message_info_kind_set", null);

            if (selected_voivodeships != null && selected_info_kinds != null) {
                if (selected_voivodeships.size() != 0 && selected_info_kinds.size() != 0) {
                    if (core != null)
                        if (core.getStatus() == AsyncTask.Status.RUNNING) {
                            core.cancel(true);
                        }
                    data = new JSONArray();
                    for (String voivodeship : selected_voivodeships) {
                        for (String info_kind : selected_info_kinds) {
                            core = new Core();
                            core.asyncResponse = this;
                            core.execute(voivodeship, info_kind, "0");
                        }
                    }
                    sharedPreferencesHasChanged = false;
                    Toast toast = Toast.makeText(this, "Aktualizuję...", Toast.LENGTH_SHORT);
                    toast.show();
                } else {
                    mRecyclerView.setAdapter(null);
                    Toast toast = Toast.makeText(this, "Wybierz poprawnie ustawienia", Toast.LENGTH_SHORT);
                    toast.show();
                }
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.action_bar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                Intent i = new Intent(this, SettingsActivity.class);
                startActivity(i);
                return true;

            case R.id.action_refresh:
                selected_voivodeships = sharedPreferences.getStringSet("message_voivodeship_set", null);
                selected_info_kinds = sharedPreferences.getStringSet("message_info_kind_set", null);

                if (selected_voivodeships != null && selected_info_kinds != null) {
                    if (selected_voivodeships.size() != 0 && selected_info_kinds.size() != 0) {
                        if (core != null)
                            if (core.getStatus() == AsyncTask.Status.RUNNING) {
                                core.cancel(true);
                            }
                        data = new JSONArray();
                        for (String voivodeship : selected_voivodeships) {
                            for (String info_kind : selected_info_kinds) {
                                core = new Core();
                                core.asyncResponse = this;
                                core.execute(voivodeship, info_kind, "0");
                            }
                        }
                        Toast toast = Toast.makeText(this, "Aktualizuję...", Toast.LENGTH_SHORT);
                        toast.show();
                        System.out.println(selected_info_kinds.size());
                        return true;
                    } else {
                        mRecyclerView.setAdapter(null);
                        Toast toast = Toast.makeText(this, "Test: Czegoś nie wybrałeś...", Toast.LENGTH_SHORT);
                        toast.show();
                    }
                }
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void processFinish(JSONArray jsonArray) {
        if (jsonArray != null) {
            try {
                data = JSONHandler.appendJSONArrays(data, jsonArray);
                mAdapter = new DataAdapter(data);
                mAdapter.setHasStableIds(true);
                mRecyclerView.swapAdapter(mAdapter, false);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}


