package com.kkulpan.rsoprojekt.rsoprojekt;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.widget.Toast;

import java.util.Date;

public class NotificationService extends Service {

    public static Runnable runnable = null;
    public static Date date = null;
    public Context context = this;
    public Handler handler = null;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        Toast.makeText(this, "Notyfikacje włączone", Toast.LENGTH_LONG).show();

        handler = new Handler();
        runnable = new Runnable() {
            public void run() {

                DownloadNewData downloadNewData = new DownloadNewData(context);
                downloadNewData.executeTask();

                handler.postDelayed(runnable, 90000);
            }
        };

        handler.postDelayed(runnable, 90000);
    }

    @Override
    public void onDestroy() {
        handler.removeCallbacks(runnable);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_STICKY;
    }

}