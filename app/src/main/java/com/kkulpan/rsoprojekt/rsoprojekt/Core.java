package com.kkulpan.rsoprojekt.rsoprojekt;

import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Core extends AsyncTask<String, Void, JSONArray> {

    AsyncResponse asyncResponse = null;
    String content = null;

    @Override
    protected void onPostExecute(JSONArray result) {
        asyncResponse.processFinish(result);
    }

    @Override
    protected JSONArray doInBackground(String... params) {
        JSONArray output = new JSONArray();
        RSOApi mealApi = new RSOApi();
        StringBuilder content = mealApi.POST(params[0], params[1], params[2]);
        try {
            JSONObject jsonObject = new JSONObject(content.toString());
            JSONArray jsonArray = jsonObject.getJSONArray("newses");
            jsonArray = JSONHandler.putVoivodeshipAndInfoKind(jsonArray, params[0], params[1]);
            output = JSONHandler.appendJSONArrays(output, jsonArray);
        } catch (JSONException e) {
            //e.printStackTrace();
        }
        return output;
    }
}
