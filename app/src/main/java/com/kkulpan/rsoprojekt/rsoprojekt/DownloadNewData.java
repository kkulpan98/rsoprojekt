package com.kkulpan.rsoprojekt.rsoprojekt;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import static com.kkulpan.rsoprojekt.rsoprojekt.DownloadAllData.CHANNEL_ID;

public class DownloadNewData implements AsyncResponse {

    private Context context;
    private AtomicInteger c = new AtomicInteger(0);

    DownloadNewData(Context context) {
        this.context = context;
    }

    void executeTask() {

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);

        String selected_voivodeship = sharedPreferences.getString("notifications_voivodeship", "wszystkie");
        String selected_info_kind = sharedPreferences.getString("notifications_info_kind", "wszystkie");

        Core core = new Core();
        core.asyncResponse = this;
        core.execute(selected_voivodeship, selected_info_kind, "0");
    }

    private int createID() {
        return c.getAndIncrement();
    }

    @Override
    public void processFinish(JSONArray jsonArray) {
        try {
            JSONArray newses = JSONHandler.getInfo(jsonArray);
            JSONObject news = newses.getJSONObject(0);
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

            if (NotificationService.date == null) {
                NotificationService.date = sdf.parse(news.getString("updated_at"));
                return;
            }
            NotificationService.date = sdf.parse(news.getString("updated_at"));
            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context, CHANNEL_ID)
                    .setSmallIcon(R.drawable.ic_notification_24dp)
                    .setContentTitle(news.getString("title"))
                    .setContentText(news.getString("shortcut"))
                    .setStyle(new NotificationCompat.BigTextStyle()
                            .bigText(news.getString("shortcut")))
                    .setPriority(NotificationCompat.PRIORITY_HIGH)
                    .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                    .setDefaults(Notification.DEFAULT_ALL);

            Intent notifyIntent = new Intent(context, InfoDashboardElement.class);

            List<String> all_values = new ArrayList<String>() {
            };
            List<String> all_keys = new ArrayList<String>() {
            };
            for (int j = 0; j < news.names().length(); j++) {
                try {
                    all_keys.add(news.names().get(j).toString());
                    all_values.add(news.getString(news.names().get(j).toString()));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                notifyIntent.putExtra("all_keys", all_keys.toArray(new String[0]));
                notifyIntent.putExtra("all_values", all_values.toArray(new String[0]));
            }

            notifyIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                    | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            PendingIntent notifyPendingIntent = PendingIntent.getActivity(
                    context, 0, notifyIntent, PendingIntent.FLAG_UPDATE_CURRENT
            );

            mBuilder.setContentIntent(notifyPendingIntent);
            NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);
            notificationManager.notify(createID(), mBuilder.build());

        } catch (JSONException | ParseException e) {
            e.printStackTrace();
        }
    }
}


