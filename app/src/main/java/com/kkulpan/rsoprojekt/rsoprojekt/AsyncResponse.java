package com.kkulpan.rsoprojekt.rsoprojekt;

import org.json.JSONArray;

public interface AsyncResponse {
    void processFinish(JSONArray jsonArray);
}

