package com.kkulpan.rsoprojekt.rsoprojekt;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.MenuItem;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.osmdroid.api.IMapController;
import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.CopyrightOverlay;
import org.osmdroid.views.overlay.Marker;

import java.util.Arrays;
import java.util.List;

public class InfoDashboardElement extends AppCompatActivity {

    private MapView.LayoutParams mapParams = new MapView.LayoutParams(
            org.osmdroid.views.MapView.LayoutParams.MATCH_PARENT,
            500,
            null, 0, 0, 0);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.info_dashboard_element_precise);
        Context context = getApplicationContext();

        setTitle("Treść komunikatu");
        buildProperLinearLayout(context, R.id.linear_layout);

        Toolbar myToolbar = findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
    }

    private void buildProperLinearLayout(Context context, int linearLayoutID) {

        LinearLayout linearLayout = findViewById(linearLayoutID);

        Intent i = getIntent();
        String[] all_keys = i.getExtras().getStringArray("all_keys");
        String[] all_values = i.getExtras().getStringArray("all_values");
        for (int j = 0; j < all_keys.length; j++) {
            String value = all_values[j];
            TextView textView = new TextView(context);
            textView.setLayoutParams(new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT));
            textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
            textView.setTextColor(Color.BLACK);
            textView.setGravity(Gravity.CENTER);
            textView.setPadding(4, 4, 4, 4);

            switch (all_keys[j]) {
                case "title":
                    textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 24);
                    break;
                case "updated_at":
                    textView.setCompoundDrawablesRelativeWithIntrinsicBounds(0, 0, R.drawable.ic_notifications_outline_black_24dp, 0);
                    textView.setGravity(Gravity.END);
                    textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
                    break;
                case "longitude":
                    continue;
                case "latitude":
                    continue;
                case "water_level_value":
                    TextView source = new TextView(context);
                    source.setText(R.string.data_source_water_level);
                    source.setGravity(Gravity.END);
                    source.setTextColor(Color.parseColor("#444444"));
                    linearLayout.addView(source);
                    textView.setTextColor(Color.parseColor("#444444"));
                    value = "Stan aktualny wody: " + value + "cm";
                    break;
                case "water_level_warning_status_value":
                    value = "Stan ostrzegawczy wody: " + value + "cm";
                    textView.setTextColor(Color.parseColor("#444444"));
                    textView.setText(value);
                    break;
                case "water_level_alarm_status_value":
                    value = "Stan alarmowy wody: " + value + "cm";
                    textView.setTextColor(Color.parseColor("#444444"));
                    break;
                case "location_name":
                    value = "Wodowskaz: " + value;
                    textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 24);
                    break;
                case "river_name":
                    value = "Stan wody - " + value;
                    textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 24);
                    break;
                case "shortcut":
                    textView.setTextColor(Color.parseColor("#666666"));
                    textView.setPadding(20, 4, 20, 4);
                    break;
                case "content":
                    textView.setPadding(15, 4, 15, 4);
                    textView.setTextColor(Color.parseColor("#444444"));
                    break;
                case "voivodeship":
                    TextView header = new TextView(this);
                    header.setPadding(15, 25, 15, 4);
                    header.setText(R.string.additional_info);
                    header.setTextColor(Color.BLACK);
                    header.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
                    header.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.ic_additional_info_black_24dp, 0, 0, 0);
                    linearLayout.addView(header);
                    textView.setPadding(15, 4, 15, 4);
                    List<String> voivodeships_values = Arrays.asList((getResources().getStringArray(R.array.voivodeship_values)));
                    List<String> voivodeships_names = Arrays.asList((getResources().getStringArray(R.array.voivodeship_names)));
                    int voivodeship_index = voivodeships_values.indexOf(value);
                    value = voivodeships_names.get(voivodeship_index);
                    textView.setTextColor(Color.parseColor("#444444"));
                    textView.setGravity(Gravity.START);
                    value = "Województwo: " + value;
                    break;
                case "info_kind":
                    List<String> info_kind_values = Arrays.asList((getResources().getStringArray(R.array.info_kinds_values)));
                    List<String> info_kind_names = Arrays.asList((getResources().getStringArray(R.array.info_kinds_names)));
                    int info_kind_index = info_kind_values.indexOf(value);
                    value = info_kind_names.get(info_kind_index);
                    textView.setPadding(15, 4, 15, 4);
                    textView.setTextColor(Color.parseColor("#444444"));
                    textView.setGravity(Gravity.START);
                    value = "Rodzaj komunikatu: " + value;
                    break;
            }
            textView.setText(value);
            linearLayout.addView(textView);
        }
        if (Arrays.asList(all_keys).contains("latitude")) {
            linearLayout.addView(createMap(context, all_values[2], all_values[3], all_values[4]));
        }
    }

    private MapView createMap(Context context, String markerTitle,
                              String latitude, String longitude) {

        MapView mapView = new MapView(context);
        mapView.setTileSource(TileSourceFactory.DEFAULT_TILE_SOURCE);

        mapView.setLayoutParams(mapParams);
        final IMapController mapController = mapView.getController();
        mapView.setEnabled(false);
        mapView.setClickable(false);
        mapView.setFocusable(false);

        mapController.setZoom(17);

        CopyrightOverlay copyrightOverlay = new CopyrightOverlay(getApplicationContext());
        copyrightOverlay.setAlignBottom(true);
        copyrightOverlay.setAlignRight(true);
        mapView.getOverlays().add(copyrightOverlay);

        final GeoPoint geoPoint = new GeoPoint(Double.parseDouble(latitude),
                Double.parseDouble(longitude));
        Marker marker = new Marker(mapView);
        marker.setTitle(markerTitle);
        marker.setPosition(geoPoint);

        mapView.getOverlays().add(marker);
        mapController.setCenter(geoPoint);
        return mapView;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}

