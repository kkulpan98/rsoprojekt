package com.kkulpan.rsoprojekt.rsoprojekt;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;

class JSONHandler {

    private static String[] water_info_values = {
            "updated_at",
            "river_name",
            "location_name",
            "longitude",
            "latitude",
            "water_level_value",
            "water_level_warning_status_value",
            "water_level_alarm_status_value",
            "water_level_trend",
            "voivodeship",
            "info_kind"
    };

    private static String[] info_values = {
            "updated_at",
            "title",
            "shortcut",
            "content",
            "voivodeship",
            "info_kind"

    };

    static JSONArray getInfo(JSONArray jsonArray) throws JSONException {

        JSONArray array = new JSONArray();

        for (int i = 0; i < jsonArray.length(); i++) {

            JSONObject jsonObject = jsonArray.getJSONObject(i);
            JSONObject object = new JSONObject();
            String json_string;
            for (String string : info_values) {
                json_string = jsonObject.getString(string);
                if (json_string.equals("null") || json_string.equals("0") || json_string.equals(" ") || json_string.equals(""))
                    continue;
                object.put(string, json_string);
            }
            if (object.length() >= info_values.length - 1)
                array.put(object);

        }
        return array;
    }

    static JSONArray putVoivodeshipAndInfoKind(JSONArray jsonArray, String voivodeship, String info_kind) throws JSONException {
        for (int i = 0; i < jsonArray.length(); i++) {

            JSONObject jsonObject = jsonArray.getJSONObject(i);
            jsonObject.put("voivodeship", voivodeship);
            jsonObject.put("info_kind", info_kind);
        }
        return jsonArray;
    }

    static JSONArray getWaterInfo(JSONArray jsonArray) throws JSONException {
        JSONArray array = new JSONArray();
        for (int i = 0; i < jsonArray.length(); i++) {

            JSONObject jsonObject = jsonArray.getJSONObject(i);
            JSONObject object = new JSONObject();
            String json_string;
            for (String string : water_info_values) {
                json_string = jsonObject.getString(string);
                if (json_string == null || json_string.equals("0") || json_string.equals(" ") || json_string.equals(""))
                    continue;
                object.put(string, json_string);
            }
            if (object.length() >= water_info_values.length - 1)
                array.put(object);
        }
        return array;
    }

    static JSONArray appendJSONArrays(JSONArray jsonArray1, JSONArray jsonArray2) throws JSONException {

        String json_string1 = jsonArray1.toString();
        String json_string2 = jsonArray2.toString();

        if (jsonArray1.length() == 0)
            return jsonArray2;

        if (jsonArray2.length() == 0)
            return jsonArray1;

        json_string1 = json_string1.substring(json_string1.indexOf("[") + 1, json_string1.lastIndexOf("]"));
        json_string2 = json_string2.substring(json_string2.indexOf("[") + 1, json_string2.lastIndexOf("]"));
        String json_string = "[" + json_string1 + "," + json_string2 + "]";

        return new JSONArray(json_string);
    }

    static JSONObject mergeJSONObjects(JSONObject... jsonObjects) throws JSONException {

        JSONObject jsonObject = new JSONObject();

        for (JSONObject temp : jsonObjects) {
            if (temp != null) {
                Iterator<String> keys = temp.keys();
                while (keys.hasNext()) {
                    String key = keys.next();
                    jsonObject.put(key, temp.get(key));
                }
            }
        }
        return jsonObject;
    }
}
