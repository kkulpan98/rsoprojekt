package com.kkulpan.rsoprojekt.rsoprojekt;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class DataAdapter extends RecyclerView.Adapter<DataAdapter.ViewHolder> {
    private JSONArray mDataset;
    private Context context;

    DataAdapter(JSONArray jsonArray) throws JSONException {
        JSONArray infoArray = JSONHandler.getInfo(jsonArray);
        JSONArray waterInfoArray = JSONHandler.getWaterInfo(jsonArray);
        mDataset = JSONHandler.appendJSONArrays(infoArray, waterInfoArray);
    }

    @NonNull
    @Override
    public DataAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent,
                                                     int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.info_dashboard_element, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        buildProperTextView(holder, position);
    }

    private void buildProperTextView(@NonNull final ViewHolder holder, int position) {

        JSONObject jsonObject = null;
        try {
            jsonObject = mDataset.getJSONObject(position);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        final LinearLayout linearLayout = holder.mView.findViewById(R.id.linear_layout);
        context = holder.mView.getContext();
        linearLayout.removeAllViews();

        if (jsonObject != null) {
            for (int i = 0; i < jsonObject.length(); i++) {

                TextView textView = new TextView(context);
                textView.setPadding(2, 2, 2, 2);
                textView.setGravity(Gravity.CENTER);
                textView.setTextColor(Color.BLACK);
                textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
                textView.setLayoutParams(new RecyclerView.LayoutParams(
                        RecyclerView.LayoutParams.MATCH_PARENT,
                        RecyclerView.LayoutParams.WRAP_CONTENT));

                String name = null;
                String data = null;

                try {
                    name = jsonObject.names().get(i).toString();
                    data = jsonObject.getString(name);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                switch (name) {
                    case "title":
                        try {
                            String info_kind = jsonObject.get("info_kind").toString();
                            switch (info_kind) {
                                case "informacje-drogowe":
                                    textView.setCompoundDrawablesRelativeWithIntrinsicBounds(
                                            R.drawable.ic_car_black_32dp, 0, 0, 0);
                                    break;

                                case "meteorologiczne":
                                    textView.setCompoundDrawablesRelativeWithIntrinsicBounds(
                                            R.drawable.ic_weather_black_32dp, 0, 0, 0);
                                    break;

                                case "hydrologiczne":
                                    textView.setCompoundDrawablesRelativeWithIntrinsicBounds(
                                            R.drawable.ic_weatherwater_black_32dp, 0, 0, 0);
                                    break;

                                case "ogolne":
                                    textView.setCompoundDrawablesRelativeWithIntrinsicBounds(
                                            R.drawable.ic_general_black_24dp, 0, 0, 0);
                                    break;

                                default:
                                    textView.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.ic_title_black_32dp, 0, 0, 0);
                                    break;
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 24);
                        break;

                    case "updated_at":
                        textView.setGravity(Gravity.END);
                        textView.setTextColor(Color.WHITE);
                        textView.setCompoundDrawablesRelativeWithIntrinsicBounds(0, 0, R.drawable.ic_notifications_white_24dp, 0);
                        textView.setBackground(ContextCompat.getDrawable(holder.mView.getContext(), R.drawable.info_dashboard_element_data_label));
                        textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
                        break;

                    case "location_name":
                        data = "Wodowskaz: " + data;
                        textView.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.ic_watermark_32px, 0, 0, 0);
                        break;

                    case "river_name":
                        data = "Stan wody - " + data;
                        textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 24);
                        textView.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.ic_water_32px, 0, 0, 0);
                        break;

                    case "shortcut":
                        textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
                        textView.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.ic_shortcut_grey_32dp, 0, 0, 0);
                        break;

                    case "voivodeship":
                        textView.setGravity(Gravity.END);
                        List<String> voivodeships_values = Arrays.asList((context.getResources().getStringArray(R.array.voivodeship_values)));
                        List<String> voivodeships_names = Arrays.asList((context.getResources().getStringArray(R.array.voivodeship_names)));
                        int voivodeship_index = voivodeships_values.indexOf(data);
                        data = voivodeships_names.get(voivodeship_index);
                        data = "Województwo: " + data;
                        break;

                    default:
                        continue;
                }
                textView.setText(data);
                linearLayout.addView(textView);
            }
            holder.mView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    JSONObject jsonObject = null;
                    try {
                        jsonObject = mDataset.getJSONObject(holder.getAdapterPosition());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    Intent i = new Intent(v.getContext(), InfoDashboardElement.class);
                    List<String> all_values = new ArrayList<String>() {
                    };
                    List<String> all_keys = new ArrayList<String>() {
                    };
                    for (int j = 0; j < jsonObject.names().length(); j++) {
                        try {
                            all_keys.add(jsonObject.names().get(j).toString());
                            all_values.add(jsonObject.getString(jsonObject.names().get(j).toString()));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        i.putExtra("all_keys", all_keys.toArray(new String[0]));
                        i.putExtra("all_values", all_values.toArray(new String[0]));
                    }
                    v.getContext().startActivity(i);
                }
            });
        }

    }

    @Override
    public int getItemCount() {
        return mDataset.length();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        View mView;

        ViewHolder(View v) {
            super(v);
            mView = v;
        }
    }
}
